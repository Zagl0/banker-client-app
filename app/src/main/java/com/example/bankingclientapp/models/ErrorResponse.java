package com.example.bankingclientapp.models;

public class ErrorResponse implements IResponse
{
    Message error;

    public ErrorResponse(Message error) {
        this.error = error;
    }

    public Message getError() {
        return error;
    }

    public void setError(Message error) {
        this.error = error;
    }

    public static class Message{
        String message;

        public Message(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    @Override
    public String toString()
    {
        return error.message;
    }
}

