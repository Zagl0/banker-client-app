package com.example.bankingclientapp.models;

public enum OperationType
{
    withdrawal, deposit, transfer
}
