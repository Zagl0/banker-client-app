package com.example.bankingclientapp.models;

public class UserResponse implements IResponse
{
    public String id;
    public int amount;
    public String name;
    public String accountNumber;
    public String identificationNumber;

    public UserResponse(String id, int amount, String name, String accountNumber, String identificationNumber) {
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.accountNumber = accountNumber;
        this.identificationNumber = identificationNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "id='" + id + '\'' +
                ", amount=" + amount +
                ", name='" + name + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                '}';
    }
}
