package com.example.bankingclientapp.models;

public class OperationDto
{
    public OperationType operationType;
    public  int amount;
    public String accountNumber;
    public String receiverAccountNumber;

    public OperationDto(OperationType operationType, int amount, String accountNumber, String receiverAccountNumber) {
        this.operationType = operationType;
        this.amount = amount;
        this.accountNumber = accountNumber;
        this.receiverAccountNumber = receiverAccountNumber.isEmpty() ? null : receiverAccountNumber;
    }

    public OperationDto(OperationType operationType, int amount, String accountNumber) {
        this.operationType = operationType;
        this.amount = amount;
        this.accountNumber = accountNumber;
        this.receiverAccountNumber = null;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getReceiverAccountNumber() {
        return receiverAccountNumber;
    }

    public void setReceiverAccountNumber(String receiverAccountNumber) {
        this.receiverAccountNumber = receiverAccountNumber;
    }
}
