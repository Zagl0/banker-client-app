package com.example.bankingclientapp.models;

public class UserDto
{
    public String name;
    public int startAmount;
    public String identificationNumber;

    public UserDto(String name, int startAmount, String identificationNumber) {
        this.name = name;
        this.startAmount = startAmount;
        this.identificationNumber = identificationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(int startAmount) {
        this.startAmount = startAmount;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
}
