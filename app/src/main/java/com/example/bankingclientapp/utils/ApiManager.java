package com.example.bankingclientapp.utils;

import android.content.Context;
import android.os.AsyncTask;
import com.example.bankingclientapp.R;
import com.example.bankingclientapp.models.ErrorResponse;
import com.example.bankingclientapp.models.IResponse;
import com.example.bankingclientapp.models.LoginDto;
import com.example.bankingclientapp.models.OperationDto;
import com.example.bankingclientapp.models.UserDto;
import com.example.bankingclientapp.models.UserResponse;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class ApiManager
{
    private final Context context;
    private final Gson gson;

    public ApiManager(Context context)
    {
        this.context = context;
        gson = new Gson();
    }

    public IResponse PostOperation(OperationDto operationDto)
    {
        IResponse response = null;
        AsyncTaskPostOperation asyncTaskPostOperation = new AsyncTaskPostOperation();
        try
        {
            response = asyncTaskPostOperation.execute(operationDto).get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }
        return response;
    }

    public IResponse PostUser(UserDto userDto)
    {
        AsyncTaskPostUser asyncTaskPostUser = new AsyncTaskPostUser();
        try
        {
            return asyncTaskPostUser.execute(userDto).get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public IResponse PostLogin(LoginDto loginDto)
    {
        AsyncTaskPostLogin asyncTaskPostLogin = new AsyncTaskPostLogin();
        try
        {
            return asyncTaskPostLogin.execute(loginDto).get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public IResponse GetUser(String identificationNumber)
    {
        AsyncTaskGetUser asyncTaskGetUser = new AsyncTaskGetUser();
        try
        {
            return asyncTaskGetUser.execute(identificationNumber).get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    public void UpdateAmount(String identificationNumber)
    {
        IResponse response = GetUser(identificationNumber);

        if(response instanceof UserResponse && response != null){
            SharedPreferencesManager.saveSharedIntSetting(context, context.getResources().getString(R.string.PREFS_AMOUNT), ((UserResponse)response).amount);
        }
    }

    private class AsyncTaskPostOperation extends AsyncTask<OperationDto, Void, IResponse>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected IResponse doInBackground(OperationDto... dto)
        {
            IResponse result = null;
            try
            {
                URL url = new URL(context.getResources().getString(R.string.API_OPERATIONS));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept","*/*");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();

                String jsonString = gson.toJson(dto[0]);
                DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                outputStream.writeBytes(jsonString);
                outputStream.flush();
                outputStream.close();

                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_SUCCESS))
                {
                    //OK
                }
                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_ERROR))
                {
                    InputStreamReader responseBodyReader = new InputStreamReader(urlConnection.getErrorStream());
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, ErrorResponse.class);
                }

                urlConnection.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                result = new ErrorResponse(new ErrorResponse.Message(e.getMessage()));
            }

            return result;
        }

        @Override
        protected void onPostExecute(IResponse result)
        {
            super.onPostExecute(result);
        }
    }

    private class AsyncTaskPostUser extends AsyncTask<UserDto, Void, IResponse>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected IResponse doInBackground(UserDto... dto)
        {
            IResponse result = null;
            try
            {
                URL url = new URL(context.getResources().getString(R.string.API_USERS));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept","*/*");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();

                String jsonString = gson.toJson(dto[0]);
                DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                outputStream.writeBytes(jsonString);
                outputStream.flush();
                outputStream.close();

                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_CREATED))
                {
                    InputStream responseBody = urlConnection.getInputStream();
                    InputStreamReader responseBodyReader = new InputStreamReader(responseBody);
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, UserResponse.class);
                }
                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_ERROR))
                {
                    InputStreamReader responseBodyReader = new InputStreamReader(urlConnection.getErrorStream());
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, ErrorResponse.class);
                }

                urlConnection.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                result = new ErrorResponse(new ErrorResponse.Message(e.getMessage()));
            }

            return result;
        }

        @Override
        protected void onPostExecute(IResponse result)
        {
            super.onPostExecute(result);
        }
    }

    private class AsyncTaskPostLogin extends AsyncTask<LoginDto, Void, IResponse>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected IResponse doInBackground(LoginDto... dto)
        {
            IResponse result = null;
            try
            {
                URL url = new URL(context.getResources().getString(R.string.API_LOGIN));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept","*/*");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();

                String jsonString = gson.toJson(dto[0]);
                DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                outputStream.writeBytes(jsonString);
                outputStream.flush();
                outputStream.close();

                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_OK))
                {
                    InputStream responseBody = urlConnection.getInputStream();
                    InputStreamReader responseBodyReader = new InputStreamReader(responseBody);
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, UserResponse.class);
                }
                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_ERROR))
                {
                    InputStreamReader responseBodyReader = new InputStreamReader(urlConnection.getErrorStream());
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, ErrorResponse.class);
                }

                urlConnection.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                result = new ErrorResponse(new ErrorResponse.Message(e.getMessage()));
            }

            return result;
        }

        @Override
        protected void onPostExecute(IResponse result)
        {
            super.onPostExecute(result);
        }
    }

    private class AsyncTaskGetUser extends AsyncTask<String, Void, IResponse>
    {
        @Override
        protected IResponse doInBackground(String... strings)
        {
            IResponse result = null;
            try
            {
                URL url = new URL(context.getResources().getString(R.string.API_USERS) + "/" + strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_OK))
                {
                    InputStream responseBody = urlConnection.getInputStream();
                    InputStreamReader responseBodyReader = new InputStreamReader(responseBody);
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, UserResponse.class);
                }
                if (urlConnection.getResponseCode() == context.getResources().getInteger(R.integer.API_NOT_FOUND))
                {
                    InputStreamReader responseBodyReader = new InputStreamReader(urlConnection.getErrorStream());
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, ErrorResponse.class);
                }

                urlConnection.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                result = new ErrorResponse(new ErrorResponse.Message(e.getMessage()));
            }

            return result;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(IResponse result)
        {
            super.onPostExecute(result);
        }
    }
}
