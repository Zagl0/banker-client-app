package com.example.bankingclientapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankingclientapp.models.IResponse;
import com.example.bankingclientapp.models.OperationDto;
import com.example.bankingclientapp.models.OperationType;
import com.example.bankingclientapp.utils.ApiManager;
import com.example.bankingclientapp.utils.SharedPreferencesManager;

public class OperationsActivity extends AppCompatActivity
{
    ApiManager apiManager;
    TextView textView3ReceiverAccount;
    EditText editTextTextReceiverAccountNumber;
    EditText editTextTextAmount;

    TextView textViewNameSurname;
    TextView textView2AccountBalance;
    TextView textView5AccountNumber;
    ConstraintLayout layoutOperations;

    int operation_type;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operations);
        apiManager = new ApiManager(this);
        operation_type = getIntent().getIntExtra("OPERATION_TYPE", -1);

        layoutOperations = findViewById(R.id.layoutOperations);

        textView3ReceiverAccount = findViewById(R.id.textView3ReceiverAccount);
        editTextTextReceiverAccountNumber = findViewById(R.id.editTextTextReceiverAccountNumber);
        editTextTextAmount = findViewById(R.id.editTextTextAmount);

        textViewNameSurname = findViewById(R.id.textViewNameSurname);
        textView2AccountBalance = findViewById(R.id.textView2AccountBalance);
        textView5AccountNumber = findViewById(R.id.textView5AccountNumber);

        setDefaultValues();

        switch (operation_type)
        {
            case -1:
            {
                onBackPressed();
                break;
            }
            case 0: //withdrawal
            {
                setTitle("Withdrawal");
                textView3ReceiverAccount.setVisibility(View.INVISIBLE);
                editTextTextReceiverAccountNumber.setVisibility(View.INVISIBLE);

                break;
            }
            case 1: //deposit
            {
                setTitle("Deposit");
                textView3ReceiverAccount.setVisibility(View.INVISIBLE);
                editTextTextReceiverAccountNumber.setVisibility(View.INVISIBLE);
                break;
            }
            case 2: //transfer
            {
                setTitle("Transfer");
                break;
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        updateAmountAction();
    }

    public void buttonConfirm(View view)
    {
        if(editTextTextAmount.getText().toString().isEmpty()) return;

        int amount = Integer.parseInt(editTextTextAmount.getText().toString());
        String receiverAccountNumber = editTextTextReceiverAccountNumber.getText().toString();
        Intent intent = new Intent(this, MessageActivity.class);

        switch (operation_type) {
            case -1: {
                Toast.makeText(this, "Application behaved unexpectedly :(", Toast.LENGTH_SHORT).show();
                break;
            }
            case 0: //withdrawal
            {
                OperationDto operationDto = new OperationDto(OperationType.withdrawal, amount, SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), ""));
                IResponse response = apiManager.PostOperation(operationDto);
                //response jest null <=> udalo sie
                if(response!=null)
                {
                    intent.putExtra("INFO",response.toString());
                }else{
                    intent.putExtra("INFO","Withdrawal successful!");
                }

                apiManager.UpdateAmount(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ""));
                startActivity(intent);

                break;
            }
            case 1: //deposit
            {
                OperationDto operationDto = new OperationDto(OperationType.deposit, amount, SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), ""));
                IResponse response = apiManager.PostOperation(operationDto);
                //response jest null <=> udalo sie
                if(response!=null)
                {
                    intent.putExtra("INFO",response.toString());
                }else{
                    intent.putExtra("INFO","Deposit successful!");
                }
                apiManager.UpdateAmount(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ""));
                startActivity(intent);
                break;
            }
            case 2: //transfer
            {
                if(!receiverAccountNumber.equals("")){
                    OperationDto operationDto = new OperationDto(OperationType.transfer, amount, SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), ""), receiverAccountNumber);
                    IResponse response = apiManager.PostOperation(operationDto);
                    //response jest null <=> udalo sie
                    if(response!=null)
                    {
                        intent.putExtra("INFO",response.toString());
                    }else{
                        intent.putExtra("INFO","Transfer successful!");
                    }
                    apiManager.UpdateAmount(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ""));
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Receiver's account number cannot be empty!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    public void buttonCancel(View view){
        startActivity(new Intent(this, ProfileActivity.class));
    }

    private void updateAmountAction(){
        apiManager.UpdateAmount(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ""));
        setDefaultValues();
    }

    public void buttonUpdateAmount(View view) {
        updateAmountAction();
    }

    private void setDefaultValues()
    {
        textViewNameSurname.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_NAME), "Name"));
        textView2AccountBalance.setText(String.valueOf(SharedPreferencesManager.readSharedIntSetting(this, getResources().getString(R.string.PREFS_AMOUNT), 0)));
        textView5AccountNumber.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), "account_nbr"));
    }
}