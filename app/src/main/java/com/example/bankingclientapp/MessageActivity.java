package com.example.bankingclientapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MessageActivity extends AppCompatActivity
{

    TextView textViewInformationOnAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        textViewInformationOnAction = findViewById(R.id.textViewInformationOnAction);
        String message = getIntent().getStringExtra("INFO");
        textViewInformationOnAction.setText(message);
    }

    public void buttonOk(View view){
        onBackPressed();
    }
}