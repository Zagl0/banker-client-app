package com.example.bankingclientapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bankingclientapp.models.ErrorResponse;
import com.example.bankingclientapp.models.IResponse;
import com.example.bankingclientapp.models.LoginDto;
import com.example.bankingclientapp.models.OperationDto;
import com.example.bankingclientapp.models.OperationType;
import com.example.bankingclientapp.models.UserDto;
import com.example.bankingclientapp.models.UserResponse;
import com.example.bankingclientapp.utils.ApiManager;
import com.example.bankingclientapp.utils.SharedPreferencesManager;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
{
    ApiManager apiManager;
    EditText editTextTextPersonName;
    EditText editTextTextPersonPesel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiManager = new ApiManager(this);

        editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        editTextTextPersonPesel = findViewById(R.id.editTextTextPersonPesel);
    }

    @Override
    public void onBackPressed(){

    }

    public void buttonLogin(View view)
    {
        String personName = editTextTextPersonName.getText().toString();
        String personPesel = editTextTextPersonPesel.getText().toString();

        if(!personName.isEmpty() && !personPesel.isEmpty())
        {
            LoginDto loginDto = new LoginDto(editTextTextPersonName.getText().toString(), editTextTextPersonPesel.getText().toString());
            IResponse response = apiManager.PostLogin(loginDto);

            if(response instanceof ErrorResponse)
            {
                Intent intent = new Intent(this, MessageActivity.class);
                intent.putExtra("INFO",response.toString());
                startActivity(intent);
            }
            else
            {
                SharedPreferencesManager.saveSharedStringSetting(this, getResources().getString(R.string.PREFS_NAME), ((UserResponse)response).name);
                SharedPreferencesManager.saveSharedIntSetting(this, getResources().getString(R.string.PREFS_AMOUNT), ((UserResponse)response).amount);
                SharedPreferencesManager.saveSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), ((UserResponse)response).accountNumber);
                SharedPreferencesManager.saveSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ((UserResponse)response).identificationNumber);

                Toast.makeText(this, "Login successful!", Toast.LENGTH_LONG).show();
                startActivity(new Intent(this, ProfileActivity.class));
            }
        }
        else
        Toast.makeText(this, "Person name or identification number cannot be empty!", Toast.LENGTH_LONG).show();
    }
}


        /*
          TEST USER POSTA
        UserDto userDto = new UserDto("Test4", 100, "test4");
        IResponse response = apiManager.PostUser(userDto);
        Toast.makeText(this, response.toString(), Toast.LENGTH_LONG).show();


        /*  TEST Operation  POSTA

        //transfer
        OperationDto operationDto1 = new OperationDto(OperationType.transfer, 1, "25368", "22831");
        IResponse response1 = apiManager.PostOperation(operationDto1);
        //response jest null <=> udalo sie
        if(response1!=null)
        {
            Toast.makeText(this, response1.toString(), Toast.LENGTH_LONG).show();
        }

        //withdrawal
        OperationDto operationDto2 = new OperationDto(OperationType.withdrawal, 1000, "25368");
        IResponse response2 = apiManager.PostOperation(operationDto2);
        //response jest null <=> udalo sie
        if(response2!=null)
        {
            Toast.makeText(this, response2.toString(), Toast.LENGTH_LONG).show();
        }

        //deposit
        OperationDto operationDto3 = new OperationDto(OperationType.deposit, 100, "25368");
        IResponse response3 = apiManager.PostOperation(operationDto3);
        //response jest null <=> udalo sie
        if(response3!=null)
        {
            Toast.makeText(this, response3.toString(), Toast.LENGTH_LONG).show();
        }

        */