package com.example.bankingclientapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.bankingclientapp.models.OperationType;
import com.example.bankingclientapp.utils.ApiManager;
import com.example.bankingclientapp.utils.SharedPreferencesManager;

public class ProfileActivity extends AppCompatActivity
{
    ApiManager apiManager;
    TextView textViewNameSurname;
    TextView textView2AccountBalance;
    TextView textView5AccountNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        apiManager = new ApiManager(this);

        textViewNameSurname = findViewById(R.id.textViewNameSurname);
        textView2AccountBalance = findViewById(R.id.textView2AccountBalance);
        textView5AccountNumber = findViewById(R.id.textView5AccountNumber);

        textViewNameSurname.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_NAME), "Name"));
        textView2AccountBalance.setText(String.valueOf(SharedPreferencesManager.readSharedIntSetting(this, getResources().getString(R.string.PREFS_AMOUNT), 0)));
        textView5AccountNumber.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), "account_nbr"));
    }

    @Override
    public void onResume(){
        super.onResume();
        updateAmountAction();
    }

    public void buttonDeposit(View view)
    {
        Intent intent = new Intent(this, OperationsActivity.class);
        intent.putExtra("OPERATION_TYPE", OperationType.deposit.ordinal());
        startActivity(intent);
    }

    public void buttonWithdrawal(View view)
    {
        Intent intent = new Intent(this, OperationsActivity.class);
        intent.putExtra("OPERATION_TYPE", OperationType.withdrawal.ordinal());
        startActivity(intent);
    }

    public void buttonTransfer(View view)
    {
        Intent intent = new Intent(this, OperationsActivity.class);
        intent.putExtra("OPERATION_TYPE", OperationType.transfer.ordinal());
        startActivity(intent);
    }

    public void buttonLogout(View view)
    {
        SharedPreferencesManager.resetSharedSettingsToDefault(this);
        startActivity(new Intent(this, MainActivity.class));
    }

    public void buttonUpdateAmountProfile(View view) {
        updateAmountAction();
    }

    private void setDefaultValues()
    {
        textViewNameSurname.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_NAME), "Name"));
        textView2AccountBalance.setText(String.valueOf(SharedPreferencesManager.readSharedIntSetting(this, getResources().getString(R.string.PREFS_AMOUNT), 0)));
        textView5AccountNumber.setText(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_ACCOUNT), "account_nbr"));
    }

    private void updateAmountAction(){
        apiManager.UpdateAmount(SharedPreferencesManager.readSharedStringSetting(this, getResources().getString(R.string.PREFS_IDENTIFICATION), ""));
        setDefaultValues();
    }
}